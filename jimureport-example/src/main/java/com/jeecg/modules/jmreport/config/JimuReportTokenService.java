package com.jeecg.modules.jmreport.config;

import org.jeecg.modules.jmreport.api.JmReportTokenServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;

/**
 * 自定义积木报表鉴权(如果不进行自定义，则所有请求不做权限控制)
 * 1.自定义获取登录token
 * 2.自定义获取登录用户
 */
@Component
public class JimuReportTokenService implements JmReportTokenServiceI {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    private String token;

    /**
     * 通过请求获取Token
     * @param request
     * @return
     */
    @Override
    public String getToken(HttpServletRequest request) {
        token = request.getParameter("token");
        if (token == null) {
            token = request.getHeader("X-Access-Token");
        }
        return token;
    }

    /**
     * 通过Token获取登录人用户名
     * @param token
     * @return
     */
    @Override
    public String getUsername(String token) {
        String loginName = null;
        try {
            String sql="SELECT config_val from pub_config where config_name='LOGINPASSWORD'";
            String password = queryField(sql);
            AesUtil.login_validate_password=password;
           String tokenJm = AesUtil.userJiemi(token);
           String[] lArr = tokenJm.split("tokensplit");
            loginName = lArr[0];
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return loginName;
    }

    /**
     * Token校验
     * @param token
     * @return
     */
    @Override
    public Boolean verifyToken(String token) {
        if(token==null || token.equals("null")){
            return false;
        }
        try {
            String sql="SELECT config_val from pub_config where config_name='LOGINPASSWORD'";
            String password = queryField(sql);
            AesUtil.login_validate_password=password;
            String pass = AesUtil.userJiemi(this.token);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     *  自定义请求头
     * @return
     */
    @Override
    public HttpHeaders customApiHeader() {
        HttpHeaders header = new HttpHeaders();
        header.add("custom-header1", "Please set a custom value 1");
        header.add("token", this.token);
        return header;
    }

    public String queryField(String sql) {
        Connection conn = null;
        Statement stmt = null;
        String returnValue = "";
        try {
            conn = getCon();
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                returnValue = rs.getString(1);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return returnValue;
    }

    private Connection getCon() throws Exception{
        return jdbcTemplate.getDataSource().getConnection();
    }
}