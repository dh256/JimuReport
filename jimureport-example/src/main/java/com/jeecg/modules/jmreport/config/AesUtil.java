package com.jeecg.modules.jmreport.config;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * 加密解密
 * @author
 */
public class AesUtil {
    public  static String login_validate_password;
    /**
     * 获取密码
     * @return
     */
    public static String getPassword(){
        String syspass = login_validate_password;
        return  syspass;
    }

    /**
     * 用户加密
     * @param user
     * @param password
     * @return
     */
    public static String userJiami(String user, String password){
        String syspass = getPassword();
        byte[] encryptResult = SecurityAES.encrypt(user + "tokensplit" + password, syspass);
        String encryptResultStr = SecurityAES.parseByte2HexStr(encryptResult);
        return encryptResultStr;
    }
    /**
     * 用户加密
     * @param user
     * @param password
     * @return
     */
    public static String userJiami(String user, String password, String yanzhengma){
        String syspass = getPassword();
        byte[] encryptResult = SecurityAES.encrypt(user + "tokensplit" + password+"tokensplit"+yanzhengma, syspass);
        String encryptResultStr = SecurityAES.parseByte2HexStr(encryptResult);
        return encryptResultStr;
    }
    public static String jiami(String str){
        String syspass = getPassword();
        byte[] encryptResult = SecurityAES.encrypt(str, syspass);
        String encryptResultStr = SecurityAES.parseByte2HexStr(encryptResult);
        return encryptResultStr;
    }
    /**
     * 用户解密
     * @param entry
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String userJiemi(String entry) throws UnsupportedEncodingException {
        String result = null;
        String syspass = getPassword();
        byte[] decryptFrom = SecurityAES.parseHexStr2Byte(entry);
        if(decryptFrom==null) return null;
        byte[] decryptResult = SecurityAES.decrypt(decryptFrom, syspass);
        if(decryptResult!=null){
            // 解密内容进行解码
            result = new String(decryptResult, "utf-8");
        }
        return result;
    }

    public static String jiemi(String entry) throws UnsupportedEncodingException {
        String result = null;
        String syspass = getPassword();
        byte[] decryptFrom = SecurityAES.parseHexStr2Byte(entry);
        if(decryptFrom==null) return null;
        byte[] decryptResult = SecurityAES.decrypt(decryptFrom, syspass);
        if(decryptResult!=null){
            // 解密内容进行解码
            result = new String(decryptResult, "utf-8");
        }
        return result;
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
//        String content = "admin_888888";
//        String password = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0dHSa2bq7dEX3t/BrOVluuP94GyA+KIQvy5K6bTs4pcz3PLhvWNsFZakJ/KrypGhYwGqG6ogqv0WDjyZbgwGF1EHA7ztIDVDSXAvBx65+nAwmL8lFoueGXdnuW9x/ZcHB+WhbDa03Cti5ddaLI7ssuseQu6odYxw6N6ZcAtjhEwIDAQAB";
//        // 加密
//        System.out.println("加密前：" + content);
//        byte[] encryptResult = encrypt(content, password);
//        String encryptResultStr = parseByte2HexStr(encryptResult);
//        System.out.println("加密后：" + encryptResultStr);
//        // 解密
//        byte[] decryptFrom = parseHexStr2Byte(encryptResultStr);
//        byte[] decryptResult = decrypt(decryptFrom, password);
//        // 解密内容进行解码
//        String result = new String(decryptResult, "utf-8");
//        System.out.println("解密后：" + result);
//        String syspass = PropertiesUtil.getConfigValue("password");
//
//        System.out.println(AesUtil.userJiami("khjl","888888"));
//
//        System.out.println(AesUtil.userJiemi("63F08C4E92320D89F8228495B5E8B510"));

        String uuid = UUID.randomUUID().toString();
        System.out.println(uuid);
        String uuid_jm = AesUtil.jiami(uuid);
        System.out.println(uuid_jm);
        System.out.println(AesUtil.jiemi(uuid_jm));

    }

}